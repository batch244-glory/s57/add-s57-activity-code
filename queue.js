let collection = [];

// Write the queue functions below.



function print(){
	// output all the elements of the queue
	return collection;
}

function enqueue(element){
	// add element to rear of queue
	collection[collection.length] = element;
	return collection;
}

function dequeue(){
	// remove element at front of queue
	let newCollection = [];

   	for (i=0; i<collection.length-1; i++){
      newCollection[i] = collection[i+1];
   	}
   	return collection = newCollection
}

function front(){
	// show element at the front
	return collection[0];
}

function size(){
	// show total number of elements
	return collection.length;
}

function isEmpty(){
	// (outputs Boolean value describing whether queue is empty or not)
	return collection.length === 0;
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};